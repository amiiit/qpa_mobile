import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class DayEvents extends StatelessWidget {
  DateTime from;
  DateTime to;

  DayEvents({this.from, this.to});

  @override
  Widget build(BuildContext context) {
    final query = r'''
          query DayEvents($from: Timestamp, $to: Timestamp) {
            occurrences(filter: {
              from: $from,
              to: $to
            }) {
              id
              start
              end
              event {
                id
                info {
                  title
                  description
                }
              }
            }
          } 
        ''';
    return Query(
        options: QueryOptions(
          documentNode: gql(query),
          variables: <String, dynamic>{
            'from': '2020-06-01',
            'to': '2020-06-30'
          },
        ),
        builder: (QueryResult result,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            return Text(result.exception.toString());
          }
          if (result.loading) {
            return Text('Loading');
          }
          List occs = result.data['occurrences'];
          return ListView.builder(
              itemCount: occs.length,
              itemBuilder: (context, index) {
                final occ = occs[index];
                return Text(occ['event']['info']['title']);
              });
        });
  }
}
